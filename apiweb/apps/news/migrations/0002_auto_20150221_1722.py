# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='colloquium',
            name='slug',
            field=models.SlugField(max_length=500),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='slug',
            field=models.SlugField(max_length=500),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='pizza',
            name='slug',
            field=models.SlugField(max_length=500),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='press',
            name='slug',
            field=models.SlugField(max_length=500),
            preserve_default=True,
        ),
    ]
