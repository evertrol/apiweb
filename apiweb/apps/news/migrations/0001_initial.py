# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import apiweb.apps.news.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Colloquium',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('title', models.CharField(default='Title not yet known', max_length=160)),
                ('text', models.TextField(blank=True)),
                ('date', models.DateField(help_text='date of the event, if any')),
                ('date_end', models.DateField(null=True, blank=True, help_text='end date of the event, if different from the start date')),
                ('time', models.TimeField(null=True, blank=True, help_text='time of the event, if any')),
                ('time_end', models.TimeField(null=True, blank=True, help_text='end time of the event, when required')),
                ('location', models.CharField(default='', max_length=80, blank=True)),
                ('teaser_text', models.TextField(blank=True, help_text='one or two short sentences for on the front page')),
                ('teaser_picture', models.ImageField(null=True, upload_to=apiweb.apps.news.models.save_teaser_picture, blank=True, help_text='Small (100x100 pixels maximum) picture for on the front page')),
                ('visible', models.BooleanField(default=True, verbose_name='visible on site', help_text='Explicitly turn visibility for an item on or off. When visibility is turned on, behaviour follows the on/off dates')),
                ('date_on', models.DateField(default='2015-02-21', help_text='date from which the item is visible on the site (inclusive)')),
                ('date_off', models.DateField(default='2016-02-21', help_text='date until which the item is visible on the site (inclusive)')),
                ('slug', models.SlugField(max_length=200, unique=True)),
                ('speaker', models.CharField(max_length=80)),
                ('affiliation', models.CharField(max_length=120, blank=True)),
            ],
            options={
                'verbose_name': 'colloquium',
                'verbose_name_plural': 'colloquia',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('title', models.CharField(default='Title not yet known', max_length=160)),
                ('text', models.TextField(blank=True)),
                ('date', models.DateField(help_text='date of the event, if any')),
                ('date_end', models.DateField(null=True, blank=True, help_text='end date of the event, if different from the start date')),
                ('time', models.TimeField(null=True, blank=True, help_text='time of the event, if any')),
                ('time_end', models.TimeField(null=True, blank=True, help_text='end time of the event, when required')),
                ('location', models.CharField(default='', max_length=80, blank=True)),
                ('teaser_text', models.TextField(blank=True, help_text='one or two short sentences for on the front page')),
                ('teaser_picture', models.ImageField(null=True, upload_to=apiweb.apps.news.models.save_teaser_picture, blank=True, help_text='Small (100x100 pixels maximum) picture for on the front page')),
                ('visible', models.BooleanField(default=True, verbose_name='visible on site', help_text='Explicitly turn visibility for an item on or off. When visibility is turned on, behaviour follows the on/off dates')),
                ('date_on', models.DateField(default='2015-02-21', help_text='date from which the item is visible on the site (inclusive)')),
                ('date_off', models.DateField(default='2016-02-21', help_text='date until which the item is visible on the site (inclusive)')),
                ('slug', models.SlugField(max_length=200, unique=True)),
                ('language', models.CharField(default='en', max_length=3, choices=[('en', 'English'), ('nl', 'Dutch')], help_text='Language that is used in the item text')),
            ],
            options={
                'verbose_name': 'event',
                'verbose_name_plural': 'events',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Pizza',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('title', models.CharField(default='Title not yet known', max_length=160)),
                ('text', models.TextField(blank=True)),
                ('date', models.DateField(help_text='date of the event, if any')),
                ('date_end', models.DateField(null=True, blank=True, help_text='end date of the event, if different from the start date')),
                ('time', models.TimeField(null=True, blank=True, help_text='time of the event, if any')),
                ('time_end', models.TimeField(null=True, blank=True, help_text='end time of the event, when required')),
                ('location', models.CharField(default='', max_length=80, blank=True)),
                ('teaser_text', models.TextField(blank=True, help_text='one or two short sentences for on the front page')),
                ('teaser_picture', models.ImageField(null=True, upload_to=apiweb.apps.news.models.save_teaser_picture, blank=True, help_text='Small (100x100 pixels maximum) picture for on the front page')),
                ('visible', models.BooleanField(default=True, verbose_name='visible on site', help_text='Explicitly turn visibility for an item on or off. When visibility is turned on, behaviour follows the on/off dates')),
                ('date_on', models.DateField(default='2015-02-21', help_text='date from which the item is visible on the site (inclusive)')),
                ('date_off', models.DateField(default='2016-02-21', help_text='date until which the item is visible on the site (inclusive)')),
                ('slug', models.SlugField(max_length=200, unique=True)),
                ('speaker', models.CharField(max_length=80)),
                ('affiliation', models.CharField(max_length=120, blank=True)),
                ('shorttalk_speaker', models.CharField(max_length=80, blank=True)),
            ],
            options={
                'verbose_name': 'pizza lunch talk',
                'verbose_name_plural': 'pizza lunch talks',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Press',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('title', models.CharField(default='Title not yet known', max_length=160)),
                ('text', models.TextField(blank=True)),
                ('date', models.DateField(help_text='date of the event, if any')),
                ('date_end', models.DateField(null=True, blank=True, help_text='end date of the event, if different from the start date')),
                ('time', models.TimeField(null=True, blank=True, help_text='time of the event, if any')),
                ('time_end', models.TimeField(null=True, blank=True, help_text='end time of the event, when required')),
                ('location', models.CharField(default='', max_length=80, blank=True)),
                ('teaser_text', models.TextField(blank=True, help_text='one or two short sentences for on the front page')),
                ('teaser_picture', models.ImageField(null=True, upload_to=apiweb.apps.news.models.save_teaser_picture, blank=True, help_text='Small (100x100 pixels maximum) picture for on the front page')),
                ('visible', models.BooleanField(default=True, verbose_name='visible on site', help_text='Explicitly turn visibility for an item on or off. When visibility is turned on, behaviour follows the on/off dates')),
                ('date_on', models.DateField(default='2015-02-21', help_text='date from which the item is visible on the site (inclusive)')),
                ('date_off', models.DateField(default='2016-02-21', help_text='date until which the item is visible on the site (inclusive)')),
                ('slug', models.SlugField(max_length=200, unique=True)),
                ('language', models.CharField(default='en', max_length=3, choices=[('en', 'English'), ('nl', 'Dutch')], help_text='Language that is used in the item text')),
            ],
            options={
                'verbose_name': 'press',
                'verbose_name_plural': 'press',
            },
            bases=(models.Model,),
        ),
    ]
