# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0002_auto_20150221_1722'),
    ]

    operations = [
        migrations.AlterField(
            model_name='colloquium',
            name='date_off',
            field=models.DateField(help_text='date until which the item is visible on the site (inclusive)'),
        ),
        migrations.AlterField(
            model_name='colloquium',
            name='date_on',
            field=models.DateField(help_text='date from which the item is visible on the site (inclusive)'),
        ),
        migrations.AlterField(
            model_name='event',
            name='date_off',
            field=models.DateField(help_text='date until which the item is visible on the site (inclusive)'),
        ),
        migrations.AlterField(
            model_name='event',
            name='date_on',
            field=models.DateField(help_text='date from which the item is visible on the site (inclusive)'),
        ),
        migrations.AlterField(
            model_name='pizza',
            name='date_off',
            field=models.DateField(help_text='date until which the item is visible on the site (inclusive)'),
        ),
        migrations.AlterField(
            model_name='pizza',
            name='date_on',
            field=models.DateField(help_text='date from which the item is visible on the site (inclusive)'),
        ),
        migrations.AlterField(
            model_name='press',
            name='date_off',
            field=models.DateField(help_text='date until which the item is visible on the site (inclusive)'),
        ),
        migrations.AlterField(
            model_name='press',
            name='date_on',
            field=models.DateField(help_text='date from which the item is visible on the site (inclusive)'),
        ),
    ]
