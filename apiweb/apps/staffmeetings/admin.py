from __future__ import unicode_literals, absolute_import, division

from django.contrib import admin
from .models import Staffmeeting, Attachment


class AttachmentsInline(admin.TabularInline):
    model = Attachment
    fk_name = "meeting"

class StaffmeetingAdmin(admin.ModelAdmin):
    list_display = ('date',)
    ordering = ('-date',)
    fields = ['date']
    inlines = [AttachmentsInline]


class AttachmentAdmin(admin.ModelAdmin):
    pass


admin.site.register(Staffmeeting, StaffmeetingAdmin)
admin.site.register(Attachment, AttachmentAdmin)
