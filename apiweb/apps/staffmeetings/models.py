from __future__ import unicode_literals, absolute_import, division

from django.db import models
from django.template.defaultfilters import slugify
from django.utils.translation import ugettext_lazy as _
import os
from datetime import datetime


def get_upload_location(instance, filename):
    filename, extension = os.path.splitext(filename)
    slug = '{}{}'.format(slugify(filename), extension)
    perm_dir = "private" if instance.permission == 0 else "public"
    date = instance.meeting.date.strftime("%Y-%m-%d")
    return os.path.join("uploads", "staff_meetings", date, perm_dir, slug)


class Staffmeeting(models.Model):
    """API staffmeeting"""

    date = models.DateField(_('date staff meeting'), unique=True)

    class Meta:
        verbose_name = _('staff meeting')
        verbose_name_plural = _('staff meetings')
        ordering = ('-date', )

    def __str__(self):
        return "Meeting {}".format(self.date)


class Attachment(models.Model):
    name = models.CharField(max_length=256)
    file = models.FileField(upload_to=get_upload_location,
                            blank=True, null=True)
    permission = models.SmallIntegerField(
        choices=((0, 'private'), (1, 'public')), default=0)
    meeting = models.ForeignKey(Staffmeeting, related_name='attachments')

    def __str__(self):
        permission = "public" if self.permission else "private"
        return "{}-{} ({})".format(self.meeting.date, self.name, permission)
