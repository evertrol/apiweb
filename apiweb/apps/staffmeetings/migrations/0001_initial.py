# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import apiweb.apps.staffmeetings.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Staffmeeting',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(unique=True, verbose_name='date staff meeting')),
                ('agenda', models.FileField(blank=True, upload_to=apiweb.apps.staffmeetings.models.get_upload_location, null=True, verbose_name='agenda')),
                ('report', models.FileField(blank=True, upload_to=apiweb.apps.staffmeetings.models.get_upload_location, null=True, verbose_name='report')),
                ('appendix_1', models.FileField(blank=True, upload_to=apiweb.apps.staffmeetings.models.get_upload_location, null=True, verbose_name='appendix_1')),
                ('appendix_2', models.FileField(blank=True, upload_to=apiweb.apps.staffmeetings.models.get_upload_location, null=True, verbose_name='appendix_2')),
                ('appendix_3', models.FileField(blank=True, upload_to=apiweb.apps.staffmeetings.models.get_upload_location, null=True, verbose_name='appendix_3')),
                ('decisions', models.FileField(blank=True, upload_to=apiweb.apps.staffmeetings.models.get_upload_location, null=True, verbose_name='decisions')),
                ('appendix_A', models.FileField(blank=True, upload_to=apiweb.apps.staffmeetings.models.get_upload_location, null=True, verbose_name='appendix_A')),
                ('appendix_B', models.FileField(blank=True, upload_to=apiweb.apps.staffmeetings.models.get_upload_location, null=True, verbose_name='appendix_B')),
            ],
            options={
                'verbose_name': 'staff meeting',
                'ordering': ('-date',),
                'verbose_name_plural': 'staff meetings',
            },
            bases=(models.Model,),
        ),
    ]
