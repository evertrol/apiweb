# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BachelorProject',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('title', models.CharField(default='', max_length=160, help_text='Title for project')),
                ('text', models.TextField(help_text='Main text')),
                ('contact', models.CharField(max_length=300, help_text='Contact person, phone, email etc')),
                ('academic_year', models.PositiveIntegerField(help_text="Starting calendar year (eg, '2008' for 2008-2009)")),
                ('date_on', models.DateField(default='2015-02-21', help_text='date from which the item is visible on the site (inclusive)')),
                ('date_off', models.DateField(default='2016-02-21', help_text='date until which the item is visible on the site (inclusive)')),
                ('visible', models.BooleanField(default=True, verbose_name='visible on site', help_text='Explicitly turn visibility for an item on or off. When visibility is turned on, behaviour follows the on/off dates')),
                ('slug', models.SlugField(unique=True, max_length=200)),
                ('year', models.PositiveSmallIntegerField(choices=[(1, 'first'), (2, 'second'), (3, 'third')], default=3)),
            ],
            options={
                'verbose_name': 'Bachelor project',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CourseTopic',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(default='', max_length=80, help_text='Name of class to be taught')),
                ('teacher', models.CharField(default='', max_length=80, help_text='  ')),
                ('description', models.TextField(blank=True)),
                ('semester', models.PositiveSmallIntegerField(choices=[(1, 'I'), (2, 'II'), (3, 'III'), (4, 'IV'), (5, 'V'), (6, 'VI')], default=1, help_text='  ')),
                ('slug', models.SlugField(unique=True, max_length=200)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MasterProject',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('title', models.CharField(default='', max_length=160, help_text='Title for project')),
                ('text', models.TextField(help_text='Main text')),
                ('contact', models.CharField(max_length=300, help_text='Contact person, phone, email etc')),
                ('academic_year', models.PositiveIntegerField(help_text="Starting calendar year (eg, '2008' for 2008-2009)")),
                ('date_on', models.DateField(default='2015-02-21', help_text='date from which the item is visible on the site (inclusive)')),
                ('date_off', models.DateField(default='2016-02-21', help_text='date until which the item is visible on the site (inclusive)')),
                ('visible', models.BooleanField(default=True, verbose_name='visible on site', help_text='Explicitly turn visibility for an item on or off. When visibility is turned on, behaviour follows the on/off dates')),
                ('slug', models.SlugField(unique=True, max_length=200)),
                ('year', models.PositiveSmallIntegerField(choices=[(4, 'fourth'), (5, 'fifth')], default=5)),
            ],
            options={
                'verbose_name': 'Master project',
            },
            bases=(models.Model,),
        ),
    ]
