# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('education', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bachelorproject',
            name='slug',
            field=models.SlugField(max_length=500),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='masterproject',
            name='slug',
            field=models.SlugField(max_length=500),
            preserve_default=True,
        ),
    ]
