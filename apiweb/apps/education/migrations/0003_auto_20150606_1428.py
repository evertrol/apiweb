# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('education', '0002_auto_20150222_0754'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bachelorproject',
            name='date_off',
            field=models.DateField(help_text='date until which the item is visible on the site (inclusive)'),
        ),
        migrations.AlterField(
            model_name='bachelorproject',
            name='date_on',
            field=models.DateField(help_text='date from which the item is visible on the site (inclusive)'),
        ),
        migrations.AlterField(
            model_name='masterproject',
            name='date_off',
            field=models.DateField(help_text='date until which the item is visible on the site (inclusive)'),
        ),
        migrations.AlterField(
            model_name='masterproject',
            name='date_on',
            field=models.DateField(help_text='date from which the item is visible on the site (inclusive)'),
        ),
    ]
