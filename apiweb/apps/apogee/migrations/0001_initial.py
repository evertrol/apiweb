# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Entry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=120, verbose_name='title', default='Practicum')),
                ('dome', models.PositiveSmallIntegerField(choices=[(1, 'Stellar Dome'), (2, 'Solar Dome'), (3, 'Observation Platform')], verbose_name='dome', default=1)),
                ('body', models.TextField(max_length=10000, verbose_name='text', default='', blank=True)),
                ('body_html', models.TextField(max_length=16000, verbose_name='actual html body', editable=False, default='', blank=True)),
                ('date', models.DateField(verbose_name='date of observing')),
                ('date_end', models.DateField(verbose_name='end date of observing', blank=True, null=True, help_text='End date of the observations, if different from the start date')),
                ('slug', models.SlugField(max_length=160, verbose_name='slug', editable=False, unique=True)),
                ('creator', models.ForeignKey(related_name='apo_creator', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'entry',
                'verbose_name_plural': 'entries',
            },
            bases=(models.Model,),
        ),
    ]
