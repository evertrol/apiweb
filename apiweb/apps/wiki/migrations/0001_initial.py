# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='WikiPage',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=256, verbose_name='URL name')),
                ('text', models.TextField(blank=True, verbose_name='text')),
                ('html', models.TextField(editable=False, blank=True, verbose_name='actual HTML')),
                ('creation_date', models.DateTimeField(auto_now_add=True, verbose_name='date created')),
                ('creation_author', models.CharField(editable=False, blank=True, max_length=160, verbose_name='created by')),
                ('modification_date', models.DateTimeField(editable=False, blank=True, null=True, verbose_name='date last modified')),
                ('modification_author', models.CharField(editable=False, blank=True, max_length=160, verbose_name='last modified by')),
                ('visits', models.IntegerField(editable=False, default=0, verbose_name='number of visits')),
                ('is_category_page', models.BooleanField(default=False, verbose_name='page is a category page')),
                ('is_visible', models.BooleanField(default=True, verbose_name='page is visible')),
            ],
            options={
                'verbose_name': 'wiki page',
                'verbose_name_plural': 'wiki pages',
            },
            bases=(models.Model,),
        ),
    ]
