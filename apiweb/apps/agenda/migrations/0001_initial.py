# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Entry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('title', models.CharField(verbose_name='title', max_length=120, default='')),
                ('is_public', models.BooleanField(verbose_name='show to others?', default=True, help_text='Let other users see your entry')),
                ('body', models.TextField(blank=True, verbose_name='text', max_length=10000, default='')),
                ('body_html', models.TextField(blank=True, verbose_name='actual html body', max_length=16000, editable=False, default='')),
                ('date', models.DateField(verbose_name='date of event')),
                ('date_end', models.DateField(blank=True, verbose_name='end date of event', help_text='End date of the event, if different from the start date', null=True)),
                ('slug', models.SlugField(verbose_name='slug', max_length=160, editable=False, unique=True)),
                ('creator', models.ForeignKey(related_name='creator', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'entry',
                'verbose_name_plural': 'entries',
            },
            bases=(models.Model,),
        ),
    ]
