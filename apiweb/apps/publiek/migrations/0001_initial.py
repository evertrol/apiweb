# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Activity',
            fields=[
                ('nr', models.PositiveSmallIntegerField(primary_key=True, serialize=False, verbose_name='number of activity')),
                ('is_in_block1', models.BooleanField(default=False, verbose_name='is activity part of block1?')),
                ('is_in_block2', models.BooleanField(default=False, verbose_name='is activity part of block2?')),
                ('is_in_block3', models.BooleanField(default=False, verbose_name='is activity part of block3?')),
                ('name', models.CharField(max_length=40, verbose_name='naam')),
                ('max_people', models.PositiveSmallIntegerField(verbose_name='maximum of people allowed for this activity')),
            ],
            options={
                'verbose_name_plural': 'activities',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Starnight',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(unique=True, verbose_name='starnight datum')),
                ('max_people', models.PositiveSmallIntegerField(verbose_name='maxium of people for this starnight')),
                ('is_registrable', models.BooleanField(help_text='If True, this night is now open for registration', default=False, verbose_name='is date open for registration?')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='StarnightApplicant',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=40, verbose_name='naam')),
                ('address', models.CharField(max_length=40, verbose_name='adres')),
                ('zipcode', models.CharField(max_length=40, verbose_name='postcode')),
                ('city', models.CharField(max_length=40, verbose_name='plaats')),
                ('email', models.EmailField(max_length=75, verbose_name='e-mail')),
                ('newsletter', models.PositiveSmallIntegerField(choices=[(0, 'Nee'), (1, 'Ja')], verbose_name='nieuwsbrief')),
                ('number', models.PositiveSmallIntegerField(choices=[(1, ' 1'), (2, ' 2'), (3, ' 3'), (4, ' 4'), (5, ' 5'), (6, ' 6'), (7, ' 7'), (8, ' 8'), (9, ' 9'), (10, '10')], default=1, verbose_name='aantal mensen')),
                ('date', models.ForeignKey(to='publiek.Starnight', related_name='datum')),
                ('slot1', models.ForeignKey(default=0, to='publiek.Activity', related_name='slot1')),
                ('slot2', models.ForeignKey(default=0, to='publiek.Activity', related_name='slot2')),
                ('slot3', models.ForeignKey(default=0, to='publiek.Activity', related_name='slot3')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
