# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Job',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('title', models.CharField(max_length=160, help_text='eg: PhD vacancy in gamma-ray bursts research', default='')),
                ('teaser', models.TextField(help_text='One or two sentences for front page, below the title')),
                ('text', models.TextField(help_text='Main advertising text')),
                ('deadline', models.DateField(help_text='Application deadline')),
                ('contact', models.CharField(max_length=300, help_text='Contact person, phone, email etc')),
                ('website', models.CharField(max_length=160, blank=True, help_text='Website with more information (eg about the research, group etc)')),
                ('date_on', models.DateField(help_text='date from which the item is visible on the site (inclusive)', default='2015-02-21')),
                ('date_off', models.DateField(help_text='date until which the item is visible on the site (inclusive)', default='2015-02-21')),
                ('visible', models.BooleanField(verbose_name='visible on site', help_text='Explicitly turn visibility for an item on or off. When visibility is turned on, behaviour follows the on/off dates', default=True)),
                ('slug', models.SlugField(max_length=200, blank=True, unique=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
