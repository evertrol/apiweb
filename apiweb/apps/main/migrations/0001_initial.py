# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Sticky',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=160)),
                ('url', models.CharField(max_length=500)),
                ('priority', models.IntegerField(choices=[(0, 'high'), (1, 'normal'), (2, 'low')], default=0, help_text='Higher priority items will be first in the list of stickies')),
                ('visible', models.BooleanField(verbose_name='visible on site', default=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
