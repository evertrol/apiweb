# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('authors', models.CharField(max_length=100, verbose_name='authors')),
                ('title', models.CharField(max_length=160, default='Title Unknown', verbose_name='title')),
                ('year', models.PositiveIntegerField(blank=True, null=True, verbose_name='year')),
                ('label', models.CharField(unique=True, max_length=40, verbose_name='label')),
                ('status', models.CharField(max_length=16, choices=[('present', 'Present'), ('missing', 'Missing'), ('on_loan', 'On Loan')], default='Present')),
            ],
            options={
                'verbose_name': 'book',
                'ordering': ('authors', 'title'),
                'verbose_name_plural': 'books',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BookCategory',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('category', models.CharField(unique=True, max_length=100, verbose_name='book category')),
            ],
            options={
                'verbose_name': 'book category',
                'ordering': ('category',),
                'verbose_name_plural': 'book categories',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PhDThesis',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('author', models.CharField(unique=True, max_length=100, verbose_name='author')),
                ('title', models.CharField(max_length=160, default='Title Unknown', verbose_name='title')),
                ('year', models.PositiveIntegerField(blank=True, null=True, verbose_name='year')),
                ('university', models.CharField(max_length=40, verbose_name='university')),
                ('status', models.CharField(max_length=16, choices=[('present', 'Present'), ('missing', 'Missing'), ('on_loan', 'On Loan')], default='Present')),
            ],
            options={
                'verbose_name': 'phd thesis',
                'ordering': ('author', 'title'),
                'verbose_name_plural': 'phd theses',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Proceeding',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('authors', models.CharField(max_length=100, verbose_name='authors')),
                ('title', models.CharField(max_length=160, unique=True, default='Title Unknown', verbose_name='title')),
                ('year', models.PositiveIntegerField(blank=True, null=True, verbose_name='year')),
                ('label', models.CharField(max_length=40, verbose_name='label')),
                ('status', models.CharField(max_length=16, choices=[('present', 'Present'), ('missing', 'Missing'), ('on_loan', 'On Loan')], default='Present')),
            ],
            options={
                'verbose_name': 'proceeding',
                'ordering': ('authors', 'title'),
                'verbose_name_plural': 'proceedings',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='book',
            name='category',
            field=models.ManyToManyField(blank=True, to='library.BookCategory', null=True, verbose_name='book category'),
            preserve_default=True,
        ),
    ]
