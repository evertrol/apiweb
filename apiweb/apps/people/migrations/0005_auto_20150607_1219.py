# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0004_auto_20150606_1447'),
    ]

    operations = [
        migrations.CreateModel(
            name='Position',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text='Name of position (e.g., director, faculty staff, postdoc, PhD student, ...)', max_length=80)),
            ],
        ),
        migrations.AddField(
            model_name='person',
            name='specification',
            field=models.CharField(help_text='Type of grant, or other indicator of funding', max_length=255, blank=True),
        ),
        migrations.AlterField(
            model_name='person',
            name='position',
            field=models.ForeignKey(blank=True, to='people.Position', null=True),
        ),
    ]
