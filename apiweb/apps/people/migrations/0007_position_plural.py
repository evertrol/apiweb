# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0006_auto_20150607_1242'),
    ]

    operations = [
        migrations.AddField(
            model_name='position',
            name='plural',
            field=models.CharField(help_text="Full plural name, if this is not a simple appended 's'", max_length=80, blank=True),
        ),
    ]
