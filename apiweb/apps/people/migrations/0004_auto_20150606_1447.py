# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0003_auto_20150606_1428'),
    ]

    operations = [
        migrations.AlterField(
            model_name='person',
            name='contact',
            field=models.ManyToManyField(related_name='contact', verbose_name='contact', to='research.ResearchTopic', blank=True),
        ),
        migrations.AlterField(
            model_name='person',
            name='research',
            field=models.ManyToManyField(related_name='interest', verbose_name='research', to='research.ResearchTopic', blank=True),
        ),
    ]
