# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0005_auto_20150607_1219'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='person',
            name='position',
        ),
        migrations.AddField(
            model_name='person',
            name='position',
            field=models.ManyToManyField(to='people.Position'),
        ),
    ]
