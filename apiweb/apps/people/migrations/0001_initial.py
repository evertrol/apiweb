# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import apiweb.apps.people.models


class Migration(migrations.Migration):

    dependencies = [
        ('research', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('show_person', models.BooleanField(default=True, verbose_name='person visible on website')),
                ('first_name', models.CharField(blank=True, max_length=40, verbose_name='first name')),
                ('prefix', models.CharField(blank=True, max_length=40, verbose_name='prefix')),
                ('last_name', models.CharField(max_length=40, verbose_name='last name')),
                ('slug', models.SlugField(unique=True, verbose_name='slug')),
                ('gender', models.PositiveSmallIntegerField(choices=[(1, 'Male'), (2, 'Female'), (3, 'Unknown')], blank=True, null=True, verbose_name='gender')),
                ('title', models.CharField(blank=True, max_length=40, verbose_name='title')),
                ('initials', models.CharField(blank=True, max_length=40, verbose_name='initials')),
                ('ads_name', models.CharField(blank=True, max_length=40, verbose_name='ads name')),
                ('address', models.CharField(blank=True, max_length=40, verbose_name='address')),
                ('zipcode', models.CharField(blank=True, max_length=40, verbose_name='zipcode')),
                ('city', models.CharField(blank=True, max_length=40, verbose_name='city')),
                ('country', models.CharField(blank=True, max_length=40, verbose_name='country')),
                ('home_phone', models.CharField(blank=True, max_length=40, verbose_name='home telephone')),
                ('work_phone', models.CharField(blank=True, max_length=40, verbose_name='work telephone')),
                ('mobile', models.CharField(blank=True, max_length=40, verbose_name='mobile')),
                ('office', models.CharField(blank=True, max_length=40, verbose_name='office')),
                ('birth_date', models.DateField(blank=True, null=True, verbose_name='birth date')),
                ('position', models.PositiveSmallIntegerField(choices=[(1, 'Director'), (2, 'Faculty Staff'), (3, 'Nova'), (4, 'Adjunct Staff'), (5, 'Postdoc'), (6, 'PhD Student'), (7, 'Emeritus'), (8, 'Guest'), (9, 'Master Student'), (10, 'Bachelor Student'), (11, 'Software Developer')], default=5, verbose_name='position')),
                ('mugshot', models.ImageField(blank=True, upload_to=apiweb.apps.people.models.get_mugshot_location, null=True, verbose_name='mugshot')),
                ('photo', models.ImageField(blank=True, upload_to=apiweb.apps.people.models.get_photo_location, null=True, verbose_name='photo')),
                ('email', models.EmailField(blank=True, max_length=75, null=True, verbose_name='email')),
                ('homepage', models.URLField(blank=True, null=True, verbose_name='homepage')),
                ('comments', models.TextField(blank=True, verbose_name='comments')),
                ('contact', models.ManyToManyField(related_name='contact', blank=True, to='research.ResearchTopic', null=True, verbose_name='contact')),
                ('research', models.ManyToManyField(related_name='interest', blank=True, to='research.ResearchTopic', null=True, verbose_name='research')),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL, related_name='person')),
            ],
            options={
                'verbose_name': 'person',
                'ordering': ('last_name', 'first_name'),
                'verbose_name_plural': 'persons',
            },
            bases=(models.Model,),
        ),
    ]
