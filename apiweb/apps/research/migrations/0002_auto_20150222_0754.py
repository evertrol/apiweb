# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('research', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='researchtopic',
            name='slug',
            field=models.SlugField(max_length=500, verbose_name='slug'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='thesis',
            name='slug',
            field=models.SlugField(max_length=500),
            preserve_default=True,
        ),
    ]
