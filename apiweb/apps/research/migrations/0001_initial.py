# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ResearchTopic',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('topic', models.CharField(verbose_name='research topic', max_length=40, unique=True)),
                ('category', models.PositiveSmallIntegerField(verbose_name='category', choices=[(1, 'Neutron stars and black holes'), (2, 'Cosmic explosions'), (3, 'Astroparticle physics'), (4, 'Planet formation and exoplanets'), (5, 'Stars, formation and evolution')], null=True, blank=True)),
                ('slug', models.SlugField(verbose_name='slug', unique=True)),
                ('picture', models.ImageField(verbose_name='picture', null=True, upload_to='uploads/images/research/topics/', blank=True)),
                ('description', models.TextField(verbose_name='description', null=True, blank=True)),
            ],
            options={
                'verbose_name': 'research topic',
                'verbose_name_plural': 'research topics',
                'ordering': ('topic',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Thesis',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('author', models.CharField(max_length=80)),
                ('gender', models.PositiveSmallIntegerField(verbose_name='gender', choices=[(1, 'Male'), (2, 'Female'), (3, 'Unknown')], null=True, blank=True)),
                ('title', models.CharField(max_length=160, default='Title Unknown')),
                ('date', models.DateField(help_text='Date of the thesis or defense')),
                ('type', models.CharField(max_length=3, default='PhD', choices=[('phd', 'PhD'), ('msc', 'Master'), ('bsc', 'Bachelor')])),
                ('url', models.URLField(help_text='UvA DARE URL or other URL to thesis', blank=True)),
                ('slug', models.SlugField(max_length=100, unique=True)),
            ],
            options={
                'verbose_name': 'thesis',
                'verbose_name_plural': 'theses',
            },
            bases=(models.Model,),
        ),
    ]
