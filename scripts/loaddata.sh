#! /bin/bash

python manage.py migrate
python manage.py migrate --database=wiki

apps="agenda apogee education jobs library main news people publiek research staffmeetings"
for app in $apps
do
	python manage.py loaddata --database=default "api/apps/${app}/fixtures/data.default.json"
done
python manage.py loaddata --database=wiki api/apps/wiki/fixtures/data.wiki.json

