#!/usr/bin/python

import sys, os
import socket
import random



# Add a custom Python path.
if socket.gethostname().startswith('waterman'):
    #sys.path.insert(0, "/home/astroweb/apiweb/apiweb/apiweb")
    sys.path.insert(0, "/home/astroweb/apiweb/apiweb/apiweb/apps")
else:
    sys.path.insert(0, "/net/taurus/scratch/martin/api-web/development")
    sys.path.insert(0, "/net/taurus/scratch/martin/api-web/development/apiweb")


# Set the DJANGO_SETTINGS_MODULE environment variable.
os.environ['DJANGO_SETTINGS_MODULE'] = "apiweb.settings.local"



from argparse import ArgumentParser
from django.contrib.auth.models import User


SYMBOLS = set("`~!@#$%^&*()_-+={}[]|\\?/<>,.;':\"")
AMBIGUOUS = set("1LlB8G60oOS5Z2QD")
UPPER = set(map(chr, range(65, 91)))
LOWER = set(map(chr, range(97, 123)))
NUMBERS = set(map(chr, range(48, 58)))
ALL = SYMBOLS|UPPER|LOWER|NUMBERS
SIMPLE = ALL-SYMBOLS
SAFE = ALL-AMBIGUOUS
SAFE_SIMPLE = SIMPLE-AMBIGUOUS


def parse_options():
    parser = ArgumentParser(description="")
    parser.add_argument("-v", "--version", action="version",
                        version="%(prog)s 0.2")
    parser.add_argument("usernames", nargs="+",
                        help="One or more user names")
    parser.add_argument("--pwlen", type=int, default=8,
                        help="Password length")
    #parser.add_argument("num_pw", type=int, nargs='?', default=10,
    #                    help="Number of passwords")
    parser.add_argument("-c", "--capitalize", action='store_true',
                        help="Level of verbose output")
    parser.add_argument("-n", "--numerals", action='store_true',
                        help="Include at least one numeral")
    parser.add_argument("-y", "--symbols", action='store_true',
                        help="Include at least one special symbol")
    parser.add_argument("-a", "--ambiguous", action='store_true',
                        help="Do not include ambiguous characters")
    args = parser.parse_args()
    return args



def run(usernames, pwlen=8, capitalize=False,
        numerals=False, symbols=False, ambiguous=True):
    if symbols:
        characters = list(ALL-AMBIGUOUS) if ambiguous else list(ALL)
    else:
        characters = (list(ALL-AMBIGUOUS-SYMBOLS) if ambiguous
                      else list(ALL-SYMBOLS))
    users = User.objects.filter(username__in=usernames)
    for user in users:
        while True:
            password = ""
            for _ in range(pwlen):
                password += random.choice(characters)
            if capitalize:
                if len(set(password) & UPPER) == 0:
                    continue
            if numerals:
                if len(set(password) & NUMBERS) == 0:
                    continue
            if symbols:
                if len(set(password) & SYMBOLS) == 0:
                    continue
            break

        user.set_password(password)
        user.save()
        sys.stdout.write('mail -s"Login details for the API website" '
                         '{} <<EOF\n\n'.format(user.email))
        sys.stdout.write("""\

Hi,

Below is your temporary login for the new API website. Please change it as
soon as possible at http://www.astro.uva.nl/internal/account/password_change/ .
For security reasons, it is best *not* to change this password to your normal
science account password (unfortunately, we also can't automatically combine
your science account login with the website login).

You can use your login to update your profile,
and to download the API phonelist.
Your profile can contain information such as your address,
your scientific interests and pictures for the digital photoboard.

You can find all this at the API internal webpages, at
http://www.astro.uva.nl/internal .

To change the contents on the website you have to access
http://www.astro.uva.nl/admin .
Here you can make a job advertisement, press release or
announce an event.


             login:  {}
temporary password:  """.format(user.username))
        sys.stdout.write("{}\n".format(password))
        sys.stdout.write("\nEOF\n\nsleep 2 \n\n")
    return 0


def main():
    args = parse_options()
    return run(args.usernames, args.pwlen, args.capitalize, args.numerals,
               args.symbols, args.ambiguous)


if __name__ == "__main__":
    sys.exit(main())
