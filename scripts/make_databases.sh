
/bin/mv -f api/databases/api.db      api/databases/api.db.bak
/bin/mv -f api/databases/api-wiki.db api/databases/api-wiki.db.bak

/usr/bin/python manage.py syncdb --database=default
/usr/bin/python manage.py syncdb --database=wiki

/usr/bin/python manage.py collectstatic

/usr/bin/setfacl -m u::rw,u:www-data:rw,g::rw,o::r api/databases/api.db
/usr/bin/setfacl -m u::rw,u:www-data:rw,g::rw,o::r api/databases/api-wiki.db
