#!/usr/bin/python

import sys, os
import socket


# Add a custom Python path.
if socket.gethostname().startswith('waterman'):
    #sys.path.insert(0, "/home/astroweb/apiweb/apiweb/apiweb")
    sys.path.insert(0, "/home/astroweb/apiweb/apiweb/apiweb/apps")
else:
    sys.path.insert(0, "/net/taurus/scratch/martin/api-web/development")
    sys.path.insert(0, "/net/taurus/scratch/martin/api-web/development/apiweb")


# Set the DJANGO_SETTINGS_MODULE environment variable.
os.environ['DJANGO_SETTINGS_MODULE'] = "apiweb.settings.local"


from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import SimpleDocTemplate, Paragraph, Table
from reportlab.lib import colors
from reportlab.lib.pagesizes import landscape, A4
from publiek.models import StarnightApplicant
from operator import itemgetter



def StarnightMaker():

# A basic document for us to write to 'starnight.pdf'
    doc = SimpleDocTemplate("starnight_with_activities.pdf",
                            pagesize=landscape(A4))
    doc.allowSplitting = 1
    doc.showBoundary = 0
    doc.leftMargin = 20.0
    doc.rightMargin = 1.0
    doc.topMargin = 1.0
    doc.bottomMargin = 1.0

# A large collection of style sheets pre-made for us
    styles = getSampleStyleSheet()

# Our container for 'Flowable' objects
    Story = []
    my_title = "Deelnemers voor de API sterrenkijkavond --- " \
               "Vrijdag 3 oktober 2014"
    Story.append(Paragraph(my_title, styles['Title']))

    my_qset = StarnightApplicant.objects.filter(
        date__date='2014-10-03').order_by('name')

    data = []
    data.append(['Nr', 'Naam', 'Aantal', '19:00 - 19:45', '20:00 - 20:45',
                 '21:00 - 21:45'])

    people = []
    for person in my_qset:
        people.append(
            {'lastname': person.name.split()[-1],
             'name': person.name,
             'aantal': person.number,
             'activ1': person.slot1,
             'activ2': person.slot2,
             'activ3': person.slot3})

# Thanks to https://wiki.python.org/moin/HowTo/Sorting#Operator_Module_Functions
#   people.sort(key=itemgetter('name'))
    people.sort(key=itemgetter('lastname'))

    k = 0
    for person in people:
        k += 1
        data.append([k, person['name'], person['aantal'],
                     person['activ1'], person['activ2'], person['activ3']])

# First the top row, with all the text centered and in Times-Bold,
# and one line above, one line below.
    ts = [('ALIGN', (0, 0), (-1, -1), 'LEFT'),
          ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
          ('LINEABOVE', (0, 0), (-1, 0), 1, colors.black),
          ('LINEBELOW', (0, 0), (-1, 0), 1, colors.black),
          #('FONT', (0, 0), (-1, 0), 'Times-Bold',  10),
          #('FONT', (0, 1), (-1, -1), 'Times-Roman', 9)]
          ('FONT', (0, 0), (-1, 0), 'Times-Roman', 10),
          ('FONT', (0, 1), (-1, -1), 'Times-Roman', 9)]

# Create the table with the necessary style, and add it to the Story list.
    table = Table(data, style=ts, splitByRow=1, repeatRows=1)
    table.canSplit = 1
    Story.append(table)

# For page break if less than 10 cm's of space is available
#   Story.append(CondPageBreak(10*cm))

# Make some space
#   Story.append(Spacer(1, 12))

# Start the construction of the pdf
# Write the document to file
    doc.build(Story)


if __name__ == "__main__":
    StarnightMaker()
    print(" PDF created ! ")
