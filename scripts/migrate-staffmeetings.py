# Three (Four) steps to migrate the staffmeetings
# (1. update to the current master)
# 2. Run this script and redirect the output to a json file:
#      migrate-staffmeetings.py > staffmeetings.json
# 3. Migrate:
#      manage.py migrate staffmeetings
# 4. Run this script again, but this time with the json file as argument (no redirection):
#      migrate-staffmeetings.py staffmeetings.json
# Done

# This script will detect whether the SQL tables in the database have
# already been migrated, and perform the correct action. It will also
# check whether there is a single argument given the second time it is
# run.

from __future__ import print_function
import os
import sys
import sqlite3
import json
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "apiweb.settings")
from apiweb import settings


def pre_migrate():
    dbname = settings.DATABASES['default']['NAME']
    with sqlite3.connect(dbname) as connection:
        cursor = connection.cursor()
        cursor.execute("select name from sqlite_master where type = 'table'")
        tables = cursor.fetchall()
        tables = [table[0] for table in tables]
        if 'staffmeetings_staffmeeting' in tables:
            cursor.execute("select sql from sqlite_master where name = 'staffmeetings_staffmeeting'")
            sql = cursor.fetchone()[0]
            sql = "".join(sql.split('\n'))
            if 'appendix_1' in sql:
                if 'staffmeetings_attachment' in tables:
                    sys.exit("Error: inconsistent database. Found "
                             "'staffmeeting.appendix_1' field and "
                             "'attachment' table in the database")
                cursor.execute("""\
SELECT id, date, agenda, report, appendix_1,
appendix_2, appendix_3, decisions,
appendix_A, appendix_B FROM
staffmeetings_staffmeeting""")
            else:
                sys.exit("'appendix_1' field not found; "
                         "has staffmeetings already been migrated?")
        else:
            sys.exit("staffmeetings_staffmeeting table not found")
        data = []
        for meeting in cursor.fetchall():
            data.append(
                {'id': meeting[0],
                 'date': meeting[1],
                 'agenda': meeting[2],
                 'report': meeting[3],
                 'appendix_1': meeting[4],
                 'appendix_2': meeting[5],
                 'appendix_3': meeting[6],
                 'decisions': meeting[7],
                 'appendix_A': meeting[8],
                 'appendix_B': meeting[9],
                 })

        print(json.dumps(data))


def post_migrate(filename):
    with open(filename) as infile:
        data = json.load(infile)

    # transform data: ids are unique
    newdata = {}
    for datum in data:
        id = datum['id']
        newdata[id] = datum.copy()

    dbname = settings.DATABASES['default']['NAME']
    with sqlite3.connect(dbname) as connection:
        cursor = connection.cursor()
        cursor.execute("select name from sqlite_master where type = 'table'")
        tables = cursor.fetchall()
        tables = [table[0] for table in tables]
        if 'staffmeetings_attachment' not in tables:
            sys.exit("staffmeeting migrations have not yet been applied")
        cursor.execute("select sql from sqlite_master where name = 'staffmeetings_staffmeeting'")
        sql = cursor.fetchone()[0]
        sql = "".join(sql.split('\n'))
        if 'appendix_1' in sql:
            sys.exit("staffmeeting migrations have not yet been applied")

        for meeting_id, data in newdata.items():
            for name, path in data.items():
                if name in ['id', 'date']:
                    continue
                if not path:  # No file; don't bother
                    continue
                if name in ['decisions', 'appendix_A', 'appendix_B']:
                    permission = 1
                else:
                    permission = 0
                cursor.execute("""
INSERT INTO staffmeetings_attachment
                (name, file, permission, meeting_id)
VALUES (?, ?, ?, ?)""", (name, path, permission, meeting_id))
        connection.commit()



if __name__ == '__main__':
    if len(sys.argv) == 1:
        pre_migrate()
    elif len(sys.argv) == 2:
        post_migrate(sys.argv[1])
    else:
        sys.exit("Run with 0 (pre-migration) or 1 (post-migration) arguments")
