#!/bin/csh

cp -f people/fixtures/initial_data.default.json           people/fixtures/initial_data.default.json.bak
cp -f jobs/fixtures/initial_data.default.json             jobs/fixtures/initial_data.default.json.bak
cp -f news/fixtures/initial_data.default.json             news/fixtures/initial_data.default.json.bak
cp -f research/fixtures/initial_data.default.json         research/fixtures/initial_data.default.json.bak
cp -f education/fixtures/initial_data.default.json        education/fixtures/initial_data.default.json.bak
cp -f staffmeetings/fixtures/initial_data.default.json    staffmeetings/fixtures/initial_data.default.json.bak
cp -f library/fixtures/initial_data.default.json          library/fixtures/initial_data.default.json.bak
cp -f publiek/fixtures/initial_data.default.json          publiek/fixtures/initial_data.default.json.bak
cp -f agenda/fixtures/initial_data.default.json           agenda/fixtures/initial_data.default.json.bak
cp -f apogee/fixtures/initial_data.default.json           apogee/fixtures/initial_data.default.json.bak
cp -f main/fixtures/initial_data.default.json             main/fixtures/initial_data.default.json.bak

cp -f wiki/fixtures/initial_data.wiki.json                wiki/fixtures/initial_data.wiki.json.bak

# tar ztvf ./json.tar.gz

tar zxvf ./json.tar.gz
