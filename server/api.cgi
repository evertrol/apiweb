#!/usr/bin/python
import sys, os

# Add a custom Python path.
sys.path.insert(0, "/home/astroweb/apiweb/api")
sys.path.insert(0, "/home/astroweb/apiweb/api/api/apps_ext")

# Switch to the directory of your project. (Optional.)
# os.chdir("/home/evert/django/api-web/development/api")

# Set the DJANGO_SETTINGS_MODULE environment variable.
os.environ['DJANGO_SETTINGS_MODULE'] = "apiweb.settings"

from django.core.servers.fastcgi import runfastcgi
runfastcgi(method="threaded", daemonize="false")
